//declarations and definiotions of variables
const butt = document.querySelectorAll('.butt');
const count = document.getElementById('count');
const cleaner = document.getElementById('cleaner');
let result = document.getElementById('result');
let plusMinus = document.getElementById('plus_minus');
let mathExpression = document.getElementById('math_expression');
let jsExpression = '';
let expressionChanged = '';
let cells = document.querySelectorAll('.calculator>.grid-x>.small-3');


//put innerText of calculator buttons to mathExpression
butt.forEach(elem => {
    elem.addEventListener('click', () => {
        //if user click "+/-" show "-"
        if (plusMinus.innerText === "+/-") {
            plusMinus.innerText = ''
        }
        //math expression showed
        mathExpression.innerText += elem.innerText;
        //change immediately plusMinnus content to earlier
        if (plusMinus.innerText === '') {
            plusMinus.innerText = '+/-'
        }
        //alter '+/-' click change sign to opposite
        if (elem.innerText == "+/-") {
            mathExpression.innerText = change_sign(mathExpression.innerText).replace('--', '')
        }

        //expressionCahnged is string understood by math.js library
        jsExpression = mathExpression.innerText;
        expressionChanged = jsExpression.replace(',', '.').replace('%x', '*0.01*').replace('x', "*").replace('%', '*0.01*');
    })
});




//show result after click "=", using mathjs library to make math expression from string
count.addEventListener('click', () => {
    //animate result
    result.animate([
        // keyframes
        {
            opacity: '0'
        },
        {
            opacity: '1'
        }
    ], {
        // timing options
        duration: 500,
        easing: 'cubic-bezier(0.42, 0, 0.58, 1)'
    });
    //if user want to count empty expression show alert
    if (expressionChanged == '') {
        alert('Type something:)')
    } else {
        try {
            //if expression is correct show result
            result.innerText = math.evaluate(expressionChanged)
            mathExpression.innerHTML = result.innerHTML
        }
        //if expression is not correct show error alert 
        catch (e) {
            alert(`Expression '${expressionChanged}' is not correct. Try another:)`)
        }

    }
})


//clear after "C" click
cleaner.addEventListener('click', () => {
    mathExpression.innerText = '';
    result.innerText = '';
})

// make buttons height equals to width
const buttonsAlwaysSquare = () => {
    cells.forEach(elem => {
        let offsetWidth = elem.offsetWidth;
        elem.style.height = offsetWidth + 'px';
        // elem.style.height = `calc(${offsetWidth}px + 2px)`;
    })
}
//run buttonsAlwaysSquare on the beginnig
buttonsAlwaysSquare();
//and run buttonsAlwaysSquare after every resize
window.addEventListener('resize', buttonsAlwaysSquare);

//function to change sign of the last number
function change_sign(v) {
    return v.replace(/[0-9]+(?!.*[0-9])/, parseInt(v.match(/[0-9]+(?!.*[0-9])/), 10) * (-1));
}