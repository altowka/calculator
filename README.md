# calculator


## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Libraries](#libreries)
* [Setup](#setup)

## General info
I've made this project as a part of recruitment process. I've had a lot of fun during doing this and I've learned some new things eg that hand-made gradients are not so bad:):)

## Technologies
Project is created with:
* JavaScript
* HTML 5
* CSS 3

## Libraries
* Foundation 6
* Mathjs

## Setup
https://altowka.gitlab.io/calculator 



